import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { RestService } from 'src/app/services/rest.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(
    private location: Location,
    private rest: RestService) { }

  ngOnInit() {
  }

  onClickCancel() {
    this.location.back();
  }

  async onClickRegister(value) {
    let result = await this.rest.register(value).toPromise();
    if (result.result == "ok") {
      this.location.back();
    } else {
      alert(result.message);
    }
  }

}
