const express = require("express");
const router = express.Router();
const Product = require("./models/product_schema");
const jwt = require("./jwt");


router.get("/product", jwt.verify, async (req, res) => {
    const doc = await Product.find();
    res.json(doc);
})


module.exports = router