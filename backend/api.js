const express = require("express");
const router = express.Router();

require("./db");
router.use(require("./api_authen"));
router.use(require("./api_transaction"));
router.use(require("./api_stock"));

module.exports = router