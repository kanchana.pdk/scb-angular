import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  private headers = new HttpHeaders({'Content-Type': 'application/json'});  
  private hostUrl = `http://localhost:8081/`;  // don't use local in case of cross domain or ip address
  private apiUrl = `${this.hostUrl}api/v2`;
  private loginUrl = `${this.apiUrl}/login`;
  private registerUrl = `${this.apiUrl}/register`; 
  private productUrl = `${this.apiUrl}/product`;  
  private transactionUrl = `${this.apiUrl}/transaction`;
  private reportTranUrl = `${this.transactionUrl}/report`;

  constructor(private http: HttpClient) { }

  login(credentail) {
    return this.http.post<any>(this.loginUrl, credentail, { headers: this.headers });
  }

  register(credentail) {
    return this.http.post<any>(this.registerUrl, credentail, { headers: this.headers });
  }

  isLogin() {
    return localStorage.getItem(environment.localAuthenInfo) != null;
  }

}
