import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestService } from 'src/app/services/rest.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isError = false;

  // DI Depencency Injection
  constructor(
    private router: Router,
    private rest: RestService) { }

  ngOnInit() {
    this.rest.isLogin() && this.router.navigate(["stock"]);
  }

  async onClickSubmit(value) {
    let result = await this.rest.login(value).toPromise()
    if (result.auth == true) {
      localStorage.setItem(environment.localAuthenInfo, result.token);
      this.isError = false;
      this.router.navigate(["stock"]);
    } else {
      this.isError = true;
    }
    console.log('local', localStorage)
  }

  onClickRegister() {
    this.router.navigate(["register"]);
  }

}
