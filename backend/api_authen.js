const express = require("express");
const router = express.Router();
const Users = require("./models/user_schema");
const bcrypt = require('bcryptjs');
const jwt = require('./jwt');

router.post("/login", async (req, res) => {
    // Destructuring
    const { username, password } = req.body;
    let doc = await Users.findOne({ username });
    if (doc == null) {
        // invalid username
        res.json({ auth: false, token: "", msg: "failed - invalid username" });
    } else if (bcrypt.compareSync(password, doc.password) == false) {
        // invalid password
        res.json({ auth: false, token: "", msg: "failed - invalid password" });
    } else {
        //login successfully
        const payload = { id: doc._id, level: doc.level, username: doc.username };
        const token = jwt.sign(payload)
        res.json({ auth: true, token, msg: "login successfully" });
    }
});

router.post("/register", async (req, res) => {
    /*
    Users.create(req.body, (error, doc) => {
        if (error) {
            res.json(error)
        } else {
            res.json(doc)
        }
    })
    */
    try {
        req.body.password = await bcrypt.hash(req.body.password, 8);
        const doc = await Users.create(req.body)
        res.json({ result: "ok", message: JSON.stringify(doc) });
    } catch(error) {
        res.json({ result: "nok", message: JSON.stringify(error) });
    }

});

module.exports = router