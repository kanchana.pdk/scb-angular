const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");

// allow angular to connect another port
app.use(cors());
// string
app.use(bodyParser.urlencoded({extended: false}));
// json
app.use(bodyParser.json());

app.use("/api/v2", require("./api"));

app.listen(8081, () => {
    console.log("Server is running ..")
})